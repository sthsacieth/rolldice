import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

public class Gui {
    int numDice = 0;
    int numSides = 20;
    public int valor = 0;
    JFrame jFrame = new JFrame();
    JPanel jPanel = new JPanel();
    JButton buttonRolar = new JButton("Rolar");
    static JLabel jLabel = new JLabel();
    public Gui(){

        buttonRolar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                rolarDados();

            }
        });
        jPanel.add(jLabel);
        jPanel.add(buttonRolar);
        jFrame.add(jPanel);

        jFrame.setVisible(true);
        jFrame.setSize(400,400);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    static void rolarDados() {
        double value = Math.random() * 20;
        DecimalFormat format = new DecimalFormat("#");
        jLabel.setText(format.format(value));

    }


}
